
import java.util.*;
import java.util.stream.Collectors;


public class Company {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int i=2;


        ArrayList<StaffMember> arrayLists = new ArrayList();
        Volunteer volunteer = new Volunteer(1,"Udom","Phnom Penh");
        arrayLists.add(volunteer);

        HourlyEmployee he = new HourlyEmployee(2,"Kim Hong","Battambong",20,0);
        arrayLists.add(he);

        SalariedEmployee se = new SalariedEmployee(3,"Si Na", "Kompet",300,150);
        arrayLists.add(se);
        First:
        do {

            Collections.sort(arrayLists, new Comparator<StaffMember>() {
                @Override
                public int compare(StaffMember o1, StaffMember o2) {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
            for (Object obj : arrayLists) {
                System.out.println("=========================================");
                System.out.println(obj);
            }
            System.out.println("=========================================");
            int option = 0;
            System.out.println("1). Add Employee\t 2). Edit\t 3). Remove\t 4).Exit\n");
            boolean isNumber=false;
            do {
                System.out.print("Choose option from(1-4): ");
                if(cin.hasNext()){
                    isNumber=true;
                    option=cin.nextInt();
                }else {
                    isNumber = false;
                    System.out.println("You must input number");
                }
            }while (!(isNumber));
            switch (option){
                case 1:
                        int ch = 0;
                        System.out.println("1). Volunteer\t 2). Hourly Emp\t 3). Salaried Emp\t 4).Back\n");
                        isNumber = false;
                        do {
                            System.out.print("Choose option (1-4) : ");
                            if (cin.hasNext()) {
                                isNumber = true;
                                ch = cin.nextInt();
                            } else {
                                isNumber = false;
                                System.out.println("You must input number");
                            }
                        } while (!(isNumber));
                        System.out.println("Current ID " + ch);
                        switch (ch) {
                            case 1:

                                i++;
                                Company.input(i);
                                Volunteer vol = new Volunteer(Company.id, Company.name, Company.address);
                                arrayLists.add(vol);
                                cin.nextLine();
                                break;
                            case 2:
                                i++;
                                Company.input(i);
                                isNumber = false;
                                int hours=0;
                                do {
                                    System.out.println("=> Enter Hours worked : ");
                                    if (cin.hasNext()) {
                                        isNumber = true;
                                         hours= cin.nextInt();
                                    } else {
                                        isNumber = false;
                                        System.out.println("You must input number");
                                    }
                                } while (!(isNumber));
                                isNumber = false;
                                double rate=0.00;
                                do {
                                    System.out.println("=> Enter rate : ");
                                    if (cin.hasNext()) {
                                        isNumber = true;
                                        rate= cin.nextInt();
                                    } else {
                                        isNumber = false;
                                        System.out.println("You must input number");
                                    }
                                } while (!(isNumber));
                                HourlyEmployee hourlyEmployee = new HourlyEmployee(Company.id,Company.name,Company.address,hours,rate);
                                arrayLists.add(hourlyEmployee);
                                break;

                            case 3:
                                i++;
                                Company.input(i);
                                isNumber = false;
                                double salary=0.00;
                                do {
                                    System.out.println("=> Enter Hours worked : ");
                                    if (cin.hasNext()) {
                                        isNumber = true;
                                        salary= cin.nextInt();
                                    } else {
                                        isNumber = false;
                                        System.out.println("You must input number");
                                    }
                                } while (!(isNumber));
                                isNumber = false;
                                double bonus=0.00;
                                do {
                                    System.out.println("=> Enter rate : ");
                                    if (cin.hasNext()) {
                                        isNumber = true;
                                        bonus= cin.nextInt();
                                    } else {
                                        isNumber = false;
                                        System.out.println("You must input number");
                                    }
                                } while (!(isNumber));
                                SalariedEmployee salariedEmployee = new SalariedEmployee(Company.id,Company.name,Company.address,salary,bonus);
                                arrayLists.add(salariedEmployee);
                                break;
                            case 4:
                                break;
                        }
                        cin.nextLine();

                    break;

                case 2:
                    System.out.println("=========EDIT INFO=======");
                    cin.nextLine();
                    int id=-1;
                    isNumber = false;
                    do {
                        System.out.print("=> Enter Employee ID to update : ");
                        if (cin.hasNext()) {
                            isNumber = true;
                            id= cin.nextInt();
                        } else {
                            isNumber = false;
                            System.out.println("You must input number");
                        }
                        if(id<0||id>arrayLists.size()){
                            System.out.println("You must enter number from 1 to "+arrayLists.size());
                        }
                    } while (id<0||id>arrayLists.size()||!(isNumber));
                    cin.nextLine();
                    int k=-1;
                    for(StaffMember obj : arrayLists){
                      k++;
                        if(obj.getId()==id){
                            System.out.println(obj);
                            System.out.print("=> Enter Staff Member's Name : ");
                            String name = cin.nextLine();
                            System.out.print("=> Enter Staff Member's Address : ");
                            String address = cin.nextLine();
                            obj = new Volunteer(id,name,address);
                            arrayLists.set(k,obj);
//                            cin.nextLine();cin.nextLine();
                            break;
                        }
                    }

                case 3:
                    System.out.println("=========DELETE INFO=======");
                    cin.nextLine();
                    id=-1;
                    isNumber = false;
                    do {
                        System.out.print("=> Enter Employee ID to delete : ");
                        if (cin.hasNext()) {
                            isNumber = true;
                            id= cin.nextInt();
                        } else {
                            isNumber = false;
                            System.out.println("You must input number");
                        }
                        if(id<0||id>arrayLists.size()){
                            System.out.println("You must enter number from 1 to "+arrayLists.size());
                        }
                    } while (id<0||id>arrayLists.size()||!(isNumber));
                    cin.nextLine();
                    k=-1;
                    for(StaffMember obj : arrayLists){
                        k++;
                        if(obj.getId()==id){
                            System.out.println(obj);
                            arrayLists.remove(k);
                            System.out.println("Remove successfully...");
                            break;
                        }
                    }
                    System.out.println("\n");
                    break;
                case 4:
                    break First;
            }
        }while (true);
    }


    static int id =0;
    static String name;
    static String address;
    //input
    public static void input(int size) {
        Scanner cin = new Scanner(System.in);
        boolean isNumber = false;

        System.out.println("==============INSERT EMPLOYEE===========");
        do {
            System.out.print("=> Enter Staff Member's ID : ");
            if (cin.hasNext()) {
                isNumber = true;
                id = cin.nextInt();
            } else {
                isNumber = false;
                System.out.println("You must input number...");
            }
            if (id <= 0 || id <=size ) {
                isNumber = false;
                System.out.print("You cannot input this id...");
            }
        } while (id <= 0 || id <= size || !(isNumber));
        System.out.print("=> Enter Staff Member's Name : ");
        cin.nextLine();
        name = cin.nextLine();
        System.out.print("=> Enter Staff Member's Address : ");
        address = cin.nextLine();

    }
}
