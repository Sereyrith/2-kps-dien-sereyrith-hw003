public class SalariedEmployee extends StaffMember {
    private double salary;
    private double bonus;

    public SalariedEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.setSalary(salary);
        this.setBonus(bonus);
    }

    @Override
    public String toString() {
        return super.toString()+"salary : " + getSalary() + "\nbonus : " + getBonus()+"\nPayment : "+pay()+"\n";
    }

    @Override
    public double pay() {

        return getSalary()+getBonus();
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }
}
