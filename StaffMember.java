import java.util.ArrayList;
import java.util.Scanner;

public abstract class StaffMember {
    private int id;
    private String name;
    private String address;

    public StaffMember(int id,String name,String address){
        this.setId(id);
        this.setName(name);
        this.setAddress(address);
    }

    //input
    Scanner cin = new Scanner(System.in);
    public void input(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ID : " + getId() + "\nName : " + getName()+ "\nAddress : " + getAddress() +"\n";
    }

    public abstract double pay();

}
