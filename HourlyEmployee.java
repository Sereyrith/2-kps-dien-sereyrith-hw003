public class HourlyEmployee extends StaffMember {
    private int houredWork;
    private double rate;

    public HourlyEmployee(int id, String name, String address, int houredWork, double rate) {
        super(id, name, address);
        this.setHouredWork(houredWork);
        this.setRate(rate);
    }

    @Override
    public String toString() {
        if(getRate()==0){
            return super.toString() +"houredWork=" + getHouredWork() +"\nrate=null"+"\nPayment=null"+"\n";
        }else {
            return super.toString() + "houredWork=" + getHouredWork() + "\nrate=" + getRate() + "\nPayment=" + pay() + "\n";
        }
    }

    @Override
    public double pay() {
        return getHouredWork()*getRate();
    }

    public int getHouredWork() {
        return houredWork;
    }

    public void setHouredWork(int houredWork) {
        this.houredWork = houredWork;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
